class SceneManager{

	// 单例模式的实现方式
	private static _manager:SceneManager;

	public static getInstance(mainScene?:egret.DisplayObjectContainer){

		if(!SceneManager._manager ){

			SceneManager._manager = new SceneManager();
			SceneManager._manager.mainScene = mainScene;

 
		} 
			return SceneManager._manager;
		
	}
	public constructor() {

	}
	private _currentScene:Scene;
	private mainScene:egret.DisplayObjectContainer; //根场景

	// 切换场景，移除旧场景
	public  changeScene(newScene:Scene){
		//移除旧场景
		if(this._currentScene){

			this.mainScene.removeChild(this._currentScene);

			this._currentScene = null;

		}

		// 添加新场景

		this.mainScene.addChild(newScene);

		this._currentScene = newScene;
	}

}