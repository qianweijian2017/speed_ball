class GameScene extends eui.Component {

	public gameGroup:eui.Group;

	private numberArr:number[];
	private bg:eui.Image;


	private _currentNumber:number =0;

	
	private rect:eui.Rect;
	public btn_start:eui.Label;
 
 	public label_clock:eui.Label;

	private timer:egret.Timer;

	// 所耗费的时间
	private _startTime:number;
	private _endTime:number;
   
	// 倒计时弹窗
	public gp_timeBox:eui.Group;
	public lab_title:eui.Label;
	public lab_time:eui.Label;

	private circleArr = [];

	 
 
	private _colorDefault = 0x608fe2;

	private _colorError = 0xf90000;

	private _colorTrue = 0x02ad4e;

	private _isTrue:boolean;
	private _isFalse:boolean;

	public constructor() {
	   	super();
 
		let numberArr = [];

		for(let i =1 ;i <= 25;i++){
			
			numberArr.push(i)
		}

		this.numberArr = numberArr;
 
		this.skinName = "resource/exml/GameSceneSkin.exml"
  		this.addEventListener(eui.UIEvent.CREATION_COMPLETE,this.onComplete,this)
		 

 

	}

	private removeCircle(){
 

			 let numChildren = this.gameGroup.numChildren;

			for(var i = 0 ; i < numChildren ;i++){

				let subCls =   <eui.Rect>this.gameGroup.$children[i];
			
					subCls.removeChildAt(0)

			}
	}

	private onTapStart(){
  
		this.gp_timeBox.visible = true;
		this.btn_start.visible = false;

		let timenumber = 3; 

		let inter = setInterval(()=>{

			timenumber  = timenumber-1;

			this.lab_time.text = timenumber+""


			if( timenumber == 0){
				// alert('done')

				clearInterval(inter);

				this.removeCircle();
				this._startTime = egret.getTimer();
				 
				this.addCircles(()=>{ 	});


				this.removeChild(this.btn_start)
 				this.gameGroup.touchChildren = true;
				this.removeChild(this.gp_timeBox)

 				this.label_clock.text =  "按1-25号的顺序依次点击糖果球"
				return;
				
			}

			


		},1000)

		  
 		this.btn_start.backgroundColor = 0xC3C1C1;
 
	}
	 
 
	private onComplete(){ 

	   this.stage.scaleMode = egret.StageScaleMode.FIXED_WIDTH;
		
		// 顶部提示文字
		this.label_clock.y = 80;

		this.label_clock.textAlign  = egret.HorizontalAlign.CENTER;
		this.label_clock.x = 0;
		this.label_clock.width = GameData.STAGEWIDTH	
		this.label_clock.scaleY = 1/App.BILIH 


		// 中间数字
		this.gameGroup.width = 600;
		this.gameGroup.height = 600;
 
		this.gameGroup.x = (GameData.STAGEWIDTH - this.gameGroup.width)/2

		this.gameGroup.y = this.label_clock.y + this.label_clock.height + 60;

		 
 		this.scaleY = App.BILIH;

		this.gameGroup.scaleY = 1/App.BILIH

 
		// 开始按钮

		this.btn_start.anchorOffsetX = this.btn_start.width/2
		this.btn_start.anchorOffsetY = this.btn_start.height/2

		this.btn_start.scaleY = 1/App.BILIH * 0.8
 
		this.btn_start.scaleX = .8
		// alert(GameData.STAGEHEIGHT);
		this.btn_start.y =   GameData.STAGEHEIGHT*(1/App.BILIH) -180; 

		
		this.btn_start.x =  GameData.STAGEWIDTH/2  ; 


		// 时间盒子

		this.gp_timeBox.scaleY = 1/App.BILIH ;

		this.gp_timeBox.width =  GameData.STAGEWIDTH ;

		this.gp_timeBox.x = 0 ;


		// 提示
		this.lab_title.width = GameData.STAGEWIDTH;

		this.lab_title.verticalCenter  = -300;
  
		this.lab_title.textAlign  = egret.HorizontalAlign.CENTER;

		this.lab_title.x = 0;


		// 数字
		this.lab_time.width = GameData.STAGEWIDTH;
		this.lab_time.verticalCenter = -100;
		 
		this.lab_time.textAlign  = egret.HorizontalAlign.CENTER;
		this.lab_time.x = 0;
 
		this.gameGroup.touchChildren = false;

		this.btn_start.visible = false;
 
  
		this.addCircles(()=>{
			
			this.addAnimation();
			
		});


		this.btn_start.addEventListener(egret.TouchEvent.TOUCH_TAP,this.onTapStart,this)
		
 
	}
	private addCircles(callback:Function){

		let randarr = App.randomArr(this.numberArr);
 		
 		for(let i = 0 ; i < 5 ; i++){ 
			
			for(let j =0 ; j < 5 ; j++){

				let circle = new Circle( randarr[ i*5+j ] );

				circle.x = i%5 * this.gameGroup.width/5;

				circle.y = j%5 * this.gameGroup.height/5;

				this.circleArr.push( circle )
	 
				this.gameGroup.addChild(circle);
 
				circle.addEventListener(egret.TouchEvent.TOUCH_TAP,this.onTapCircle,this)
				

			}
 

		}

		callback && callback();
 
	} 
 

	private timeBox(scene,seconds:number){

        let group:eui.Group = new eui.Group();
		

    }
 
	private addAnimation(){
		
		for( let i = 24;i >= 0; i-- ){

			let circle =  this.gameGroup.getChildAt(i);

			circle.y = -( circle.y + this.stage.stageHeight/2 );
 
		}

		let maxTime = 0;

		for( let i = 0;i <= 4; i++ ){
 
			for( let j = 0;j <= 4; j++ ){
 
					let circle = this.gameGroup.getChildAt( i*5+j );

					let time  = ( i*(5-i)+(5-j) );

					let waitTime = 200* (time-1);

					maxTime = maxTime  >  waitTime ? maxTime :waitTime;

					let tween = egret.Tween.get(circle)
							.wait( waitTime )
							.to( { y : -circle.y - this.stage.stageHeight/2 }, 2000,egret.Ease.backIn );
 
 

			}
		}
		// console.log(maxTime+)


		setTimeout(()=> {
			
			 this.btn_start.visible = true;
			
		}, maxTime+2400);

		  
	}
	private removeEvent(){

			for(let i = 0 ;i < this.circleArr.length;i++){
				this.circleArr[i].removeEventListener(egret.TouchEvent.TOUCH_TAP,this.onTapCircle,this)
				
			}
	}
	
	private done(){
 

					  
		this._endTime = egret.getTimer();

		let seconds = (this._endTime -this._startTime);

		this.removeEvent();
 
 		let score = seconds/1000;

	 
			App.Post({
					do:'play',
					params:{  
						score:  score , 
						sign : App.getSign(score),
						openid : App.getUser().openid,
				},
				} ,(data)=>{ 
				
				if(data.code==1){

						let playparams = data.data;

						let user = App.getUser(); 

						user.result = playparams;
 						
						App.setUser(user)

						location.href = Config.H5_FILYIN
  
   
						// let gameover:GameOver  = new GameOver();

						// this.addChild(gameover) 


						this.gameGroup.parent && this.gameGroup.parent.removeChild(this.gameGroup)
						this.btn_start.parent && this.btn_start.parent.removeChild(this.btn_start)
						this.lab_time.parent && this.lab_time.parent.removeChild(this.lab_time)
						this.label_clock.parent && this.label_clock.parent.removeChild(this.label_clock)

						

				}else{

					alert(data.message)
				}

				

			})



 
   

	} 

	 private onTapCircle(e:egret.Event){
 
		let group = <eui.Group>e.currentTarget.getChildAt(0)
		let yesbg = <eui.Rect>group.getChildAt(0);
		let nobg = <eui.Rect>group.getChildAt(1);
		let bg = <eui.Rect>group.getChildAt(2);

		// this.done();
		 
		
		 
		let label = <eui.Label>group.getChildAt(3)
		
		if(yesbg.name== this._currentNumber+"" )return;
 
 
		if( Number(label.text) == this._currentNumber + 1){
			
			this._currentNumber +=1;

				let numChildren = this.gameGroup.numChildren;
 
				for(var i = 0 ; i < numChildren ;i++){

					let subCls =   <eui.Group>this.gameGroup.$children[i];

					let yesbg = <eui.Rect>subCls['yes'];  
					let nobg = <eui.Rect>subCls['no'];  
					let cbg = <eui.Rect>subCls['bg'];  
					let num_label = <eui.Label>subCls['num_label'];  
				  num_label.textColor =0x458FEB
  					
					yesbg.visible = false;
					nobg.visible = false; 
					cbg.visible = true;

				}
				
				label.textColor =0xffffff


		  bg.visible = false;
		
			yesbg.visible =true;
 
			yesbg.name = label.text
  

			if( this._currentNumber == 25){
				 
				this.done();

			}
 
 
		}else{

		 let numChildren = this.gameGroup.numChildren;
			
				for(var i = 0 ; i < numChildren ;i++){

					let subCls =   <eui.Group>this.gameGroup.$children[i];

 					let cnobg = <eui.Rect>subCls['no'];
 					let cbg = <eui.Rect>subCls['bg'];
 					let ybg = <eui.Rect>subCls['yes'];

					let num_label = <eui.Label>subCls['num_label'];  
 
					if(!ybg.visible){
					 cbg.visible = true; 
				 	 num_label.textColor =0x458FEB

					}

 					 cnobg.visible = false; 
 
				}


 				nobg.visible = true;
				bg.visible = false;
				label.textColor =0xffffff
 			
			  
			
		}

 

	}

 
//当场景在舞台上移除
	protected  onRemove(){

	}
}

 