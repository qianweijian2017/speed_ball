class initGame extends eui.Component{


	public dot0:eui.Rect;
	public dot1:eui.Rect;
	public dot2:eui.Rect;
	public dot3:eui.Rect;
	public box_l:eui.Image;
	public box_r:eui.Image;
	private yArr:any[] =[];

	private box_rotation:any[] = [];

	public gp_start:eui.Group;

	public gp_done:eui.Group;

	private playparams;


	private _times=0;//耗时



	public constructor() {
		super();


  		this.addEventListener(eui.UIEvent.CREATION_COMPLETE,this.onComplete,this)

 
		this.skinName = "resource/exml/initGameSkin.exml"

		
	}

	private onComplete(){

	
		this.openBox();

	}

	private openBox(){

		 let box_l = egret.Tween.get(this.box_l)
	 	 let box_r =  egret.Tween.get(this.box_r)

		 this.box_rotation =  [ this.box_l.rotation , this.box_r.rotation]
 

		box_l.to({ rotation:0 },1800).call(()=>{
			this.moveTopDot();
		});
		box_r.to({ rotation:0 },1800);

	}

	private moveTopDot(){
 
		for(let i = 0 ; i < 4 ;i++){

			let dot = this['dot'+i];

			this.yArr.push(dot.y)
			
	 		egret.Tween.get(dot)
			.to({ y: dot.y-300 },1500+i*200).wait(1000).call(()=>{
 

			}) 

				


		}

	 setTimeout(()=> {
				
				let gameScene = new GameScene(); 

				this.addChild(gameScene);


		}, 3000);


	}

	private closeBox(){

		 let box_l = egret.Tween.get(this.box_l)
	 	 let box_r =  egret.Tween.get(this.box_r)

		 box_l.to({ rotation: this.box_rotation[0] },1800) 
		 box_r.to({ rotation: this.box_rotation[1] },1800)
		 	  .call(()=>{
				   	console.log('计算完成')

					   this.isDraw();


			   })

		for(let i = 0 ; i < 4 ;i++){

			let dot = this['dot'+i];
			
				egret.Tween.get(dot)
					.to({ y: this.yArr[i] },1500+i*200).call(()=>{
 			}) 


			}
 

	}

	// 是否可以抽奖,跳h5页面

	private isDraw(){ 

 		let params = `openid=${ window.localStorage.getItem("openid")}&score_text=${this.playparams.score_text}&score=${this.playparams.score}&suipian=${this.playparams.suipian}`
 
			 
  		window.location.href =  Config.H5_RESULT +`?`+params



	}

	// 传送秒数

	private  checkDraw(){
				 
			App.Post({
					do:'play',
					params:{ 
					openid: window.localStorage.getItem('openid'),
					score:  this._times ,
				},
				} ,(data)=>{


				if(data.code==1){

						this.playparams = data.data;


						
					//  alert(`用时 ${ this._times } s,段位:${data.data.score_text}`)

				}else{

					alert(data.message)
				}

				

			})
			
	}
	public moveBottomDot(seconds:number){

		this._times = seconds/1000;

		this.checkDraw();
 
		this.gp_start.visible= false;
		this.gp_done.visible=  true;

	    this.closeBox();
		  

	 	
	}



}