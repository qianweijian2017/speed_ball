class StartScene extends eui.Component{


	public startBtn:eui.Button;

	public constructor() {
		super();

 
		this.addEventListener(eui.UIEvent.CREATION_COMPLETE,this.onComplete,this)
		this.skinName = "resource/exml/StartSceneSkin.exml"

		

	}

	//当场景添加到舞台上
	protected  onComplete(){ 

 		this.startBtn.addEventListener(egret.TouchEvent.TOUCH_TAP,this.onTapBtn,this)
		 
	}
	private onTapBtn(){

		this.startBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP,this.onTapBtn,this)

  		let gameScene = new GameScene(); 

		this.addChild(gameScene);

		// if(this1.parent){	

		// 	this1.parent.removeChild(this1);
		// }
			
 
	}
//当场景在舞台上移除
	protected  onRemove(){

	}
}
window['StartScene'] = StartScene;
