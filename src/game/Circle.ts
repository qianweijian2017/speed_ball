class Circle extends eui.Component{

 
 		public num_label:eui.Label;
		private _currentNumber:number =0;

		public circle_box:eui.Group;
		
		public bg:eui.Rect;
 

		private _colorDefault = 0x608fe2;
		private _colorError = 0xf5bd01;
		private _colorTrue = 0xff2100;

  		private _number:string;
		  

		public constructor(num:number) {
			super();
 
			this.addEventListener(eui.UIEvent.CREATION_COMPLETE,this.onComplete,this)
			this.skinName = "resource/exml/CircleSkin.exml" 
			this._number = num + "";
  
	
		}
		private onComplete(){

 			this.num_label.text = this._number ; 
 
 		}
	

 
}

window['Circle'] = Circle;
