abstract class Scene extends egret.DisplayObjectContainer{
	public constructor() {
		super();
		this.addEventListener(egret.Event.ADDED_TO_STAGE,this.onAdd,this);
		this.addEventListener(egret.Event.REMOVED_FROM_STAGE,this.onRemove,this)
	}
	protected abstract onAdd(); //当场景添加到舞台上

	protected abstract onRemove(); //当场景在舞台上移除

 	
}