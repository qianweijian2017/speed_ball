class SoundManager {

	// 声明一些变量
	// 背景音乐
	private bgSound: egret.Sound;
	// 游戏房间的音乐
	private gameSound: egret.Sound;
	// 背景音乐的声道
	private bgChannel: egret.SoundChannel;
	// 游戏房间的声道
	private gameChannel: egret.SoundChannel;

	private static _instance: SoundManager;
	public static instance(): SoundManager{
		return this._instance == null ? this._instance = new SoundManager() : this._instance;
	}
	public constructor() {
	}
	/**
	 * 初始化背景音乐
	 */
	public initBgSound(){
		if(this.bgChannel){
			this.bgChannel.stop();
			this.bgChannel = null;
		}
		this.bgSound = RES.getRes("game_bg_mp3");
	} 

}