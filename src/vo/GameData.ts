class GameData {
	//屏幕的宽高
	public static STAGEWIDTH: number = 0;
	public static STAGEHEIGHT: number = 0;
	//预设的宽高
	public static PreinstallWidth: number = 720;
	public static PreinstallHeight: number = 1280;


	//屏幕的适配方法
	public static adaptiveScreen(contentGroup: eui.Group | egret.Sprite | eui.Image): void {
		if (!contentGroup) {
			return;
		}
		contentGroup.x = GameData.STAGEWIDTH / 2;
		contentGroup.y = GameData.STAGEHEIGHT / 2;
		contentGroup.anchorOffsetX = contentGroup.width / 2;
		contentGroup.anchorOffsetY = contentGroup.height / 2;
		//计算宽高比
		let scaleValue: number = Math.min((GameData.STAGEWIDTH / contentGroup.width), (GameData.STAGEHEIGHT / contentGroup.height), 1);
		//保留一位小数
		scaleValue = Math.floor(scaleValue * 10) / 10;
		//进行容器缩放
		contentGroup.scaleX = contentGroup.scaleY = scaleValue;




	}
	 
	/**
	 * 是否在游戏中 1:表示在游戏中  -1:表示不再游戏中
	 */
	public static isGaming: number = -1;
	/**
	 * 是否播放高潮音乐
	 */
	public static playGaoChaoSound: boolean = false;
	/**
	 * 是否停止音效
	 */
	public static stopEffect: boolean = false;

}