//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////

class Main extends eui.UILayer {

    private mc1;


    protected createChildren(): void {
        super.createChildren();

        //记录屏幕的宽高
        GameData.STAGEWIDTH = this.stage.stageWidth;
        GameData.STAGEHEIGHT = this.stage.stageHeight;

        // this.initWx();

        egret.lifecycle.addLifecycleListener((context) => {
            // custom lifecycle plugin
        })

        // egret.lifecycle.onPause = () => {
        //     egret.ticker.pause();
        // }

        // egret.lifecycle.onResume = () => {
        //     egret.ticker.resume();
        // }

        //inject the custom material parser
        //注入自定义的素材解析器
        let assetAdapter = new AssetAdapter();
        egret.registerImplementation("eui.IAssetAdapter", assetAdapter);
        egret.registerImplementation("eui.IThemeAdapter", new ThemeAdapter());

        //注入我们自己自定义的素材解析器
        RES.processor.map("swf", new starlingswf.SwfAnalyzer());
 
        this.runGame().catch(e => {
            console.log(e);
        })
    }

    private async runGame() {
        await this.loadResource()
        this.createGameScene();
        // const result = await RES.getResAsync("description_json")
        // this.startAnimation(result);
        await platform.login();
        const userInfo = await platform.getUserInfo();
        console.log(userInfo);

    }

    private initWx(){

        App.Post({
            do : 'weixinsdk',
            params:{
                url :  'egret/index.html' 
            }
        },(data)=>{
            // alert()
            
            if(data.code==1){
                // alert(JSON.stringify(data.data))

                var bodyConfig: BodyConfig = new BodyConfig();
                    bodyConfig.debug = false; 
                    bodyConfig.appId = data.data.appId;
                    bodyConfig.timestamp = data.data.timestamp;
                    bodyConfig.nonceStr = data.data.nonceStr;
                    bodyConfig.signature = data.data.signature;
                    bodyConfig.jsApiList =  [ //需要调用的JS接口列表
                           'checkJsApi',
                           'onMenuShareTimeline', //分享给好友
                           'onMenuShareAppMessage', //分享到朋友圈 

      
                      ] 
 
                    
                    /// ... 其他的配置属性赋值
                    /// 通过config接口注入权限验证配置
                    if(wx) {
                        // alert('wx')
                        wx.config(bodyConfig);
                        wx.ready(()=> {
                        /// 在这里调用微信相关功能的 API
                          //分享给朋友
                              this.onShareAPPMessage();
                              this.onMenuShareTimeline();



                        });
                    }



            }else{

            }



        })


        
    }
    private onShareAPPMessage() { 
            var shareAppMessage = new BodyMenuShareAppMessage(); 

            
        
            shareAppMessage.title = '大卖潜力测试';
            shareAppMessage.desc = '跨境圈藏龙卧虎，挖掘你的大卖潜力';
            shareAppMessage.link = 'http://pingpong.centralsoft.cn';
            shareAppMessage.imgUrl = 'http://pingpong.centralsoft.cn/html5/share.png';
            shareAppMessage.trigger = function (res) {
                // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                console.log('用户点击发送给朋友');
            }    
            shareAppMessage.success = function (res) {
                console.log('已分享');
            };
            shareAppMessage.fail = function (res) {
                console.log('已取消');
            };
            shareAppMessage.cancel = function (res) {
                console.log(JSON.stringify(res));
            };   
        }


   private onMenuShareTimeline(): void { 
        var sharet = new BodyMenuShareTimeline(); 
         sharet.title = '大卖潜力测试';
        // sharet.desc = '跨境圈藏龙卧虎，挖掘你的大卖潜力';
        sharet.link = 'http://pingpong.centralsoft.cn';
        sharet.imgUrl = 'http://pingpong.centralsoft.cn/html5/share.png';
        sharet.trigger = function (res) {
            // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
            console.log('用户点击分享到朋友圈');
        };
        sharet.success = function (res) {
            console.log('已分享');
        };
        sharet.cancel = function (res) {
            console.log('已取消');
        };
        sharet.fail = function (res) {
            console.log(JSON.stringify(res));
        };    
    }

    private async loadResource() {
        try {
            //首先加载preload 资源用来组成loadingView页面
            await RES.loadConfig("resource/default.res.json", "resource/");
            await this.loadTheme();
            
             // 先加载loading所需要的资源
            RES.createGroup("preload",["preload"]);

            await RES.loadGroup("preload",0);

             const  loadingView = await new LoadingUI();
            this.stage.addChild(loadingView);
            //使用loadingView来加载项目的主资源（test)
            await RES.loadGroup("game", 0,loadingView);

            // this.stage.removeChild(loadingView);
        }
        catch (e) {
            console.error(e);
        }
    }

    private loadTheme() {
        return new Promise((resolve, reject) => {
            // load skin theme configuration file, you can manually modify the file. And replace the default skin.
            //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
            let theme = new eui.Theme("resource/default.thm.json", this.stage);
            theme.addEventListener(eui.UIEvent.COMPLETE, () => {
                resolve();
            }, this);

        })
    }

    private textfield: egret.TextField;
    /**
     * 创建场景界面
     * Create scene interface
     */
    protected createGameScene(): void {
 
      
        App.reGame = Number(egret.getOption('reGame') || '0' );

    }

   
}
