module starlingswf {    //解析swf格式
    export class SwfAnalyzer implements RES.processor.Processor {
        constructor() {
        }
        public async onLoadStart(host, resource) {
            let data = await host.load(resource, RES.processor.JsonProcessor);
            return new starlingswf.Swf(data);
        }
        public onRemoveStart(host, request) {
            return Promise.resolve();
        }
    }
}