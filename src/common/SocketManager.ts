class SocketManager {


	private static webSocket;
	private static callback;

	private static _msg:string;

	public constructor() {

 	}

	private static getInstance(){

		if(SocketManager.webSocket){

			SocketManager.close();

		}

		return new egret.WebSocket();


	}

	private static  close(){

		SocketManager.webSocket.close();

 	}

	 
	private static connect():void {    
 
		this.webSocket = SocketManager.getInstance();    

		this.webSocket.addEventListener(egret.ProgressEvent.SOCKET_DATA, this.onReceiveMessage, this); 

		this.webSocket.addEventListener(egret.Event.CONNECT, this.onSocketOpen, this);    

		this.webSocket.connect("echo.websocket.org", 80);

	}
	private static onSocketOpen():void {    
 
		console.log("连接成功，发送数据：" );

		 
		
		
	}

	private static send(cmd,callback){

		this.callback = callback;

		this.webSocket.writeUTF(cmd);

	}


	private static onReceiveMessage(e:egret.Event):void {   

		var msg = this.webSocket.readUTF();  

		this.callback(msg);


 	}
}