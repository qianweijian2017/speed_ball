class AlertTimer extends  eui.Component {

	private _timer:egret.Timer;
	private _seconds:number;
	public lab_title:eui.Label;
	public lab_time:eui.Label;


	public constructor(seconds:number) {
			super();
			this._seconds = seconds;

			this.addEventListener(eui.UIEvent.CREATION_COMPLETE,this.onComplete,this)
			this.skinName = "resource/exml/base/AlertTimerSkin.exml" 
	}

	private onComplete(){
  
		let timer = new egret.Timer(1000,this._seconds)
		timer.addEventListener(egret.TimerEvent.TIMER,this.onTimer,this);
		timer.addEventListener(egret.TimerEvent.TIMER_COMPLETE,this.onStop,this);

		this._timer = timer;

	}

	private onTimer(e:egret.TimerEvent){

		let target = <egret.Timer>e.currentTarget;

		this.lab_time.text = target.repeatCount -   target.currentCount+"";



	}

	public start(){

		this._timer.start(); 
	}
	public onStop(callback){

  
		this._timer.removeEventListener(egret.TimerEvent.TIMER,this.onTimer,this);
		this._timer.removeEventListener(egret.TimerEvent.TIMER_COMPLETE,this.onStop,this);
		this._timer = null;

		if(this.parent){
			this.parent.removeChild(this)
		}
		

	}
}