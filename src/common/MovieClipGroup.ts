// TypeScript file
/**
 * MvoieClipGroup
 */
class MovieClipGroup extends eui.Group {
    public source:string=""; //注意：[color=#000][font=微软雅黑,]自定义组建的属性要有默认值，才能被运行时exml解析器识别[/font][/color]
 
    public constructor() {
        super();
    }
 
    protected createChildren():void {
        super.createChildren();
 
        var data = RES.getRes(this.source+"_json");
        var txtr = RES.getRes(this.source+"_png");
        var mcFactory:egret.MovieClipDataFactory = new egret.MovieClipDataFactory( data, txtr );
        var mc1:egret.MovieClip = new egret.MovieClip( mcFactory.generateMovieClipData( this.source) );
        mc1.play(-1);
        this.addChild(mc1);
    }
}