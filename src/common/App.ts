// TypeScript file

// 公共方法

class App {

    private static urlloader:egret.URLLoader =null;
    private static callback:any;

    private static test:boolean;






    private static _initGame:initGame = null;
    private static _gameGame:GameScene = null;

    public static reGame = 0;

    public static BILIW = 0; //屏幕宽比例
    public static BILIH = 0; //屏幕高比例



    constructor(){
         App.callback = null;
    }

    private static getUrlloader(){

        if(this.urlloader){
            return App.urlloader;
        }

        this.urlloader = new egret.URLLoader();

        return this.urlloader;
 
    }

    public static getUser(){

        return JSON.parse(window.localStorage.getItem('user'));
    }
    public static setUser(data){

        return window.localStorage.setItem('user',JSON.stringify(data));
    }

    public static getSign(score){

       
        let str = score +  App.getUser().openid +  Config.POST_KEY ;
        let mdSign =  new md5().hex_md5(str);
        

        return mdSign.substr(5,6);
    }

    

    // 开始请求接口
    public static Post(obj,callback?){
         
        obj = obj || {};

  
        App.callback = callback||false;

        let dos = obj.do.includes('http') ?  obj.do :  ( Config.REQUEST_URL+ obj.do );

        let url:string = dos ;

        let param = obj.params || {}
 
        let loader:egret.URLLoader = App.getUrlloader();
 
        loader.addEventListener(egret.Event.COMPLETE, this.onPostComplete, this);


            // get请求
       for (var key in param) {
            if (param.hasOwnProperty(key)) {
                var element = param[key]; 

                url += '&' + key +'='+element

                    
            }
        }
        // url += '&openid=oL6vown5JjH2LtdrD4zSjSALvSQY'
        
  
        let request:egret.URLRequest = new egret.URLRequest(url);

        request.method = obj.method =='get' ?  egret.URLRequestMethod.GET : egret.URLRequestMethod.POST;

        request.method =  egret.URLRequestMethod.GET ;
 

        let variables = ""
        let ands = "";
        if(!obj.method){

            for (var key in param) {
                if (param.hasOwnProperty(key)) {
                    var element = param[key]; 

                    variables += ands+ key +'='+element

                    ands = "&"
                    
                }
            }
        } 
        
        App.test = !!obj.test

        if(App.test){

            console.log('参数')
            console.log(param)

        }


        request.data = new egret.URLVariables(variables);

        loader.load(request);
 
    }
    // 当完成请求时的回调
    private static onPostComplete(event:egret.Event):void
    {
        
        var loader:egret.URLLoader = <egret.URLLoader> event.target;
        var data:egret.URLVariables = loader.data; 

        data = typeof data =='string'? JSON.parse(data):data;

         if(App.test){

            console.log('返回')
            console.log(data) 
        }

        App.callback &&  App.callback(data);
        
         
    }

    public static randomArr(arr){

		var t;
		for(var i = 0;i < arr.length; i++){

 			var rand = (Math.random() * arr.length).toFixed(0);
             
			t = arr[rand];
			arr[rand] =arr[i];
			arr[i] = t;
		}

		return arr.filter(v=>v);

	}

    public static initGameInstance(){

            if(!App._initGame){

                App._initGame =  new initGame();
                
            }

            return App._initGame; 

    }

    public static getGameScene(){

            if(!App._gameGame){

                App._gameGame =  new GameScene();
                
            }

            return App._gameGame; 

    }

    





}