//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////

class LoadingUI extends egret.Sprite implements RES.PromiseTaskReporter {

    public constructor() {
        super();
        this.createView();
    }
    //提示信息  声明loadingUI所需要的组件
    private textField: egret.TextField;
    private bg:egret.Bitmap;
    private logo:egret.Bitmap;
    private progressbar:egret.Bitmap;
    private barWidth:number;
    private contentGroup:egret.Sprite;
    private barBg:egret.Bitmap;

    private bigTitle:egret.TextField;
    private smallTitle:egret.TextField;
    // 开始
    private btnStart:egret.Bitmap;
    private initT_png:egret.Bitmap;


    private createView(): void {
        //初始化背景图
        let bitmap:egret.Bitmap = new egret.Bitmap(RES.getRes('bg_png'));
        bitmap.width = GameData.STAGEWIDTH;
        bitmap.height = GameData.STAGEHEIGHT;
        // bitmap.scale9Grid =  
        //  if(!App.getUser() || !App.getUser().openid){
        //        alert('开始游戏错误,无法获取信息');
        //        return;
        //    }

    
        this.addChild(bitmap);
        this.bg = bitmap;

        //容器
        this.contentGroup = new egret.Sprite();
        this.addChild(this.contentGroup);
        this.contentGroup.width = GameData.PreinstallWidth;
        this.contentGroup.height = GameData.PreinstallHeight;

        GameData.adaptiveScreen(this.contentGroup);

        App.BILIW  = GameData.STAGEWIDTH/GameData.PreinstallWidth;
        App.BILIH  =GameData.STAGEHEIGHT/ GameData.PreinstallHeight;

 
        //logo
        // this.logo = new egret.Bitmap(RES.getRes('logo13_png'));
        // this.logo.x = (GameData.PreinstallWidth - this.logo.width)/2;
        // this.logo.y = (GameData.PreinstallHeight - this.logo.height)/2 - 70;
        // this.contentGroup.addChild(this.logo);

        // alert(GameData.STAGEHEIGHT)
         //初始化进度条
        this.initProgressBar();
         //初始化提示文字
        this.textField = this.createTextField(0xffffff,'加载中',GameData.STAGEHEIGHT *0.77+7,26);
        this.textField.width = GameData.STAGEWIDTH;
        this.textField.textAlign = 'center';
        this.addChild(this.textField);

        this.bigTitle = this.createTextField(0xffffff,'跨境圈瞬息万变 速度决定一切',GameData.STAGEHEIGHT*.88,30);
        this.bigTitle.width = GameData.STAGEWIDTH;
        this.bigTitle.textAlign = 'center';
        this.addChild(this.bigTitle);

        this.smallTitle = this.createTextField(0xffffff,'超级大卖的反应速度通常在20s以内',GameData.STAGEHEIGHT*.93,30);
        this.smallTitle.width = GameData.STAGEWIDTH;
        this.smallTitle.textAlign = 'center';
        this.addChild(this.smallTitle);
        
        
         
 
        // 顶部标题图片
        
        let initT_png:egret.Bitmap = new egret.Bitmap(RES.getRes('initT_png')); 
 
        // initT_png.localToGlobal() 
         
        initT_png.x = GameData.STAGEWIDTH/2 
        initT_png.anchorOffsetX = initT_png.width/2
        initT_png.anchorOffsetY = initT_png.height/2
        initT_png.y = 100 + initT_png.height/2
  
        initT_png.scaleX = .9;
        initT_png.scaleY = .9;
         
        this.addChild(initT_png);

        this.initT_png = initT_png;

         
        

    }
    public removeBarAddBtn(){


            this.textField.visible = false;
            this.progressbar.visible = false;
            this.barBg.visible = false;
            this.addStart()
            // alert('3')

            this.removeChild(this.textField);
            this.removeChild(this.progressbar);
            this.removeChild(this.barBg);



            this.progressbar =null;
            this.barBg =null;
            this.textField =null;

 


             
     }
     private addStart(){

               let btn_start:egret.Bitmap = new egret.Bitmap(RES.getRes('btngame_png')); 
                this.addChild(btn_start);
                
                btn_start.x = GameData.STAGEWIDTH /2
                btn_start.y = GameData.STAGEHEIGHT*0.78  
                // alert(GameData.PreinstallWidth)

        
                btn_start.anchorOffsetX = btn_start.width/2
                btn_start.anchorOffsetY =  btn_start.height/2

                
                // btn_start.visible = false;

                btn_start.scaleX = 0.8;
                btn_start.scaleY = 0.8;
        

                this.btnStart = btn_start;
                this.btnStart.touchEnabled = true;
                this.btnStart.addEventListener(egret.TouchEvent.TOUCH_TAP,this.btnTap,this)
                

     }
     private btnTap(e:egret.TouchEvent){

         let loading =  document.getElementById('loading')
         loading && (loading.style.display ='none');
         

           this.btnStart.removeEventListener(egret.TouchEvent.TOUCH_TAP,this.btnTap,this)
  
            if(App.reGame){

                    let game:GameScene = new GameScene();

                    this.addChild(game)
            }else{

                    let one:oneBanner = new oneBanner();

                    this.addChild(one)
            }

 

            this.btnStart.parent && this.btnStart.parent.removeChild(this.btnStart);
            this.initT_png.parent && this.initT_png.parent.removeChild(this.initT_png);
            this.smallTitle.parent && this.smallTitle.parent.removeChild(this.smallTitle);
            this.bigTitle.parent && this.bigTitle.parent.removeChild(this.bigTitle);
            this.bg.parent && this.bg.parent.removeChild(this.bg);
            // this.parent && this.parent.removeChild(this.textField);
 
			this.contentGroup.parent && this.contentGroup.parent.removeChild(this.contentGroup)


 
 
                        
                         
    }
    public onProgress(current: number, total: number): void {
        // this.textField.text = `Loading...${current}/${total}`;
        const num:number = Math.floor(100*current/total);
        this.textField.text = '加载中...';
        let width:number = this.barWidth * current/total;
        

        //设置遮罩
        if(this.progressbar.mask){
            this.progressbar.mask.width = width;
            this.progressbar.mask = this.progressbar.mask;
        }else{
            this.progressbar.mask = new egret.Rectangle(0,0,width,this.progressbar.height+20);
        }

        if(num == 100){
            // 进度条完成后
            this.removeBarAddBtn();
            return;
        }

    }
    //进度条
    private initProgressBar(){
        //底槽
        let bitmap:egret.Bitmap = new egret.Bitmap(RES.getRes('barBg_png'));
        this.addChild(bitmap);

        bitmap.height = 40;
        bitmap.width = GameData.STAGEWIDTH*0.6;

        // bitmap.anchorOffsetX = bitmap.width/2
        // bitmap.anchorOffsetY = bitmap.height/2

        bitmap.x = (GameData.STAGEWIDTH - bitmap.width)/2
        bitmap.y = GameData.STAGEHEIGHT *0.77;
        this.barBg = bitmap;

        //进度条
        this.progressbar = new egret.Bitmap(RES.getRes('barImg_png'));

        this.progressbar.height = 30;
        this.progressbar.width =  GameData.STAGEWIDTH*0.6-10;

        // this.progressbar.anchorOffsetX = this.progressbar.width/2
        // this.progressbar.anchorOffsetY = this.progressbar.height/2

        this.progressbar.x = (GameData.STAGEWIDTH  - this.progressbar.width)/2 +5
        this.progressbar.y = GameData.STAGEHEIGHT *0.77+5;

        this.addChild(this.progressbar);
        //记录进度条的宽度
        this.barWidth = this.progressbar.width;
        
    }
    //声明一个工厂方法，来生成一些TextField对象
    private createTextField(color:number,test:string,y:number,size:number = 22){
        let textField:egret.TextField = new egret.TextField();
        textField.text = test;
        textField.textColor = color;
        textField.size = size;
        // textField.anchorOffsetX = textField.width/2;
        // textField.x = GameData.PreinstallWidth / 2;
        textField.y = y;
        return textField;
    }
}
