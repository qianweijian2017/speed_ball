var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var oneBanner = (function (_super) {
    __extends(oneBanner, _super);
    function oneBanner() {
        var _this = _super.call(this) || this;
        _this.skinName = "resource/exml/oneBannerSkin.exml";
        _this.addEventListener(eui.UIEvent.CREATION_COMPLETE, _this.onComplete, _this);
        return _this;
    }
    oneBanner.prototype.mcMove = function (mcname, action, fn) {
        //  //!!!! 这里写动画,动画执行完成后,打开GameScene;
        var data = RES.getRes(mcname + "_json");
        var txtr = RES.getRes(mcname + "_png");
        var mcFactory = new egret.MovieClipDataFactory(data, txtr);
        this.mc1 = new egret.MovieClip(mcFactory.generateMovieClipData(mcname));
        //this.addChild(this.mc1);
        this.mc1.x = 0;
        // console.log(this.mc1.width	)
        // console.log(GameData.STAGEWIDTH)
        // console.log(GameData.STAGEHEIGHT)
        this.mc1.scaleX = this.bili;
        this.mc1.scaleY = this.bili;
        this.mc1.y = this.bgup.height - 1;
        // console.log(this.mc1.height);
        this.bgdown.y = this.bgup.y + this.bgup.height + this.mc1.height * this.bili - 1;
        this.addChild(this.mc1);
        this.mc1.gotoAndPlay(action, 1);
        this.mc1.addEventListener(egret.Event.COMPLETE, function (e) {
            fn();
        }, this);
    };
    oneBanner.prototype.moveBox = function () {
    };
    oneBanner.prototype.onComplete = function () {
        var _this = this;
        this.stage.scaleMode = egret.StageScaleMode.EXACT_FIT;
        this.scaleY = GameData.STAGEHEIGHT / this.stage.height;
        this.bili = GameData.STAGEWIDTH / 500;
        var bgBili = GameData.STAGEWIDTH / this.bgup.width;
        var bgBiliH = GameData.STAGEHEIGHT / this.bgup.height;
        this.bgup.width = this.bgup.width * bgBili;
        this.bgup.height = this.bgup.height * bgBili;
        this.bgup.y = 0;
        //  alert( App.BILIH)
        //  this.scaleY = App.BILIH 
        this.bgdown.width = this.bgdown.width * bgBili;
        // this.scaleY = App.BILIH
        this.mcMove('movebox', 'action', function () {
            _this.mcMove('flygo', 'action', function () {
                _this.scaleY = 1;
                var game = new GameScene();
                _this.addChild(game);
                _this.bgup.parent && _this.bgup.parent.removeChild(_this.bgup) && (_this.bgup = null);
                _this.bgdown.parent && _this.bgdown.parent.removeChild(_this.bgdown) && (_this.bgdown = null);
                _this.mc1.parent && _this.mc1.parent.removeChild(_this.mc1) && (_this.mc1 = null);
                // this.parent && this.parent.removeChild(this)
            });
        });
    };
    oneBanner.prototype.openInitGame = function () {
        // let initgame = App.initGameInstance();
        // this.addChild(initgame)
    };
    return oneBanner;
}(eui.Component));
__reflect(oneBanner.prototype, "oneBanner");
