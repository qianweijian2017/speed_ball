var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var SceneManager = (function () {
    function SceneManager() {
    }
    SceneManager.getInstance = function (mainScene) {
        if (!SceneManager._manager) {
            SceneManager._manager = new SceneManager();
            SceneManager._manager.mainScene = mainScene;
        }
        return SceneManager._manager;
    };
    // 切换场景，移除旧场景
    SceneManager.prototype.changeScene = function (newScene) {
        //移除旧场景
        if (this._currentScene) {
            this.mainScene.removeChild(this._currentScene);
            this._currentScene = null;
        }
        // 添加新场景
        this.mainScene.addChild(newScene);
        this._currentScene = newScene;
    };
    return SceneManager;
}());
__reflect(SceneManager.prototype, "SceneManager");
