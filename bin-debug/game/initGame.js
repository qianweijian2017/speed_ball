var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var initGame = (function (_super) {
    __extends(initGame, _super);
    function initGame() {
        var _this = _super.call(this) || this;
        _this.yArr = [];
        _this.box_rotation = [];
        _this._times = 0; //耗时
        _this.addEventListener(eui.UIEvent.CREATION_COMPLETE, _this.onComplete, _this);
        _this.skinName = "resource/exml/initGameSkin.exml";
        return _this;
    }
    initGame.prototype.onComplete = function () {
        this.openBox();
    };
    initGame.prototype.openBox = function () {
        var _this = this;
        var box_l = egret.Tween.get(this.box_l);
        var box_r = egret.Tween.get(this.box_r);
        this.box_rotation = [this.box_l.rotation, this.box_r.rotation];
        box_l.to({ rotation: 0 }, 1800).call(function () {
            _this.moveTopDot();
        });
        box_r.to({ rotation: 0 }, 1800);
    };
    initGame.prototype.moveTopDot = function () {
        var _this = this;
        for (var i = 0; i < 4; i++) {
            var dot = this['dot' + i];
            this.yArr.push(dot.y);
            egret.Tween.get(dot)
                .to({ y: dot.y - 300 }, 1500 + i * 200).wait(1000).call(function () {
            });
        }
        setTimeout(function () {
            var gameScene = new GameScene();
            _this.addChild(gameScene);
        }, 3000);
    };
    initGame.prototype.closeBox = function () {
        var _this = this;
        var box_l = egret.Tween.get(this.box_l);
        var box_r = egret.Tween.get(this.box_r);
        box_l.to({ rotation: this.box_rotation[0] }, 1800);
        box_r.to({ rotation: this.box_rotation[1] }, 1800)
            .call(function () {
            console.log('计算完成');
            _this.isDraw();
        });
        for (var i = 0; i < 4; i++) {
            var dot = this['dot' + i];
            egret.Tween.get(dot)
                .to({ y: this.yArr[i] }, 1500 + i * 200).call(function () {
            });
        }
    };
    // 是否可以抽奖,跳h5页面
    initGame.prototype.isDraw = function () {
        var params = "openid=" + window.localStorage.getItem("openid") + "&score_text=" + this.playparams.score_text + "&score=" + this.playparams.score + "&suipian=" + this.playparams.suipian;
        window.location.href = Config.H5_RESULT + "?" + params;
    };
    // 传送秒数
    initGame.prototype.checkDraw = function () {
        var _this = this;
        App.Post({
            do: 'play',
            params: {
                openid: window.localStorage.getItem('openid'),
                score: this._times,
            },
        }, function (data) {
            if (data.code == 1) {
                _this.playparams = data.data;
                //  alert(`用时 ${ this._times } s,段位:${data.data.score_text}`)
            }
            else {
                alert(data.message);
            }
        });
    };
    initGame.prototype.moveBottomDot = function (seconds) {
        this._times = seconds / 1000;
        this.checkDraw();
        this.gp_start.visible = false;
        this.gp_done.visible = true;
        this.closeBox();
    };
    return initGame;
}(eui.Component));
__reflect(initGame.prototype, "initGame");
