var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var GameOver = (function (_super) {
    __extends(GameOver, _super);
    function GameOver() {
        var _this = _super.call(this) || this;
        _this.skinName = "resource/exml/GameOverSkin.exml";
        _this.addEventListener(eui.UIEvent.CREATION_COMPLETE, _this.onComplete, _this);
        return _this;
    }
    GameOver.prototype.mcMove = function (mcname, action, fn) {
        //  //!!!! 这里写动画,动画执行完成后,打开GameScene;
        var data = RES.getRes(mcname + "_json");
        var txtr = RES.getRes(mcname + "_png");
        var mcFactory = new egret.MovieClipDataFactory(data, txtr);
        this.mc1 = new egret.MovieClip(mcFactory.generateMovieClipData(mcname));
        //this.addChild(this.mc1);
        this.mc1.x = 0;
        this.mc1.scaleX = GameData.STAGEWIDTH / 500;
        this.mc1.scaleY = GameData.STAGEWIDTH / 500;
        this.mc1.y = this.bgup.height;
        this.lab_done.width = this.stage.width;
        this.lab_done.textAlign = 'center';
        // console.log(this.mc1.height);
        this.bgdown.y = this.mc1.y + this.mc1.height * (GameData.STAGEWIDTH / 500);
        // this.bgdown.height = GameData.STAGEHEIGHT - this.bgdown.y;
        this.scaleY = this.height / this.stage.height;
        this.addChild(this.mc1);
        this.mc1.gotoAndPlay(action, 1);
        this.mc1.addEventListener(egret.Event.COMPLETE, function (e) {
            fn();
        }, this);
    };
    GameOver.prototype.moveBox = function () {
    };
    GameOver.prototype.onComplete = function () {
        var _this = this;
        this.stage.scaleMode = egret.StageScaleMode.EXACT_FIT;
        // this.lab_done.horizontalCenter = 0 ;
        // alert(this.lab_done.width)
        // this.lab_done.x = (this.lab_done.parent.width - this.lab_done.width)/2
        this.bili = this.bgup.width / GameData.STAGEWIDTH;
        this.bgup.width = GameData.STAGEWIDTH;
        //   this.bgup.height = this.bgup.height*this.bili;
        this.bgdown.width = GameData.STAGEWIDTH;
        this.mcMove('flyin', 'action', function () {
            // this.scaleY  = 1;
            _this.mc1 = null;
            // alert('3')
            window.location.href = Config.H5_RESULT;
        });
    };
    GameOver.prototype.openInitGame = function () {
        // let initgame = App.initGameInstance();
        // this.addChild(initgame)
    };
    return GameOver;
}(eui.Component));
__reflect(GameOver.prototype, "GameOver");
