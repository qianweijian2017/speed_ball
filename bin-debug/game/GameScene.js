var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var GameScene = (function (_super) {
    __extends(GameScene, _super);
    function GameScene() {
        var _this = _super.call(this) || this;
        _this._currentNumber = 0;
        _this.circleArr = [];
        _this._colorDefault = 0x608fe2;
        _this._colorError = 0xf90000;
        _this._colorTrue = 0x02ad4e;
        var numberArr = [];
        for (var i = 1; i <= 25; i++) {
            numberArr.push(i);
        }
        _this.numberArr = numberArr;
        _this.skinName = "resource/exml/GameSceneSkin.exml";
        _this.addEventListener(eui.UIEvent.CREATION_COMPLETE, _this.onComplete, _this);
        return _this;
    }
    GameScene.prototype.removeCircle = function () {
        var numChildren = this.gameGroup.numChildren;
        for (var i = 0; i < numChildren; i++) {
            var subCls = this.gameGroup.$children[i];
            subCls.removeChildAt(0);
        }
    };
    GameScene.prototype.onTapStart = function () {
        var _this = this;
        this.gp_timeBox.visible = true;
        this.btn_start.visible = false;
        var timenumber = 3;
        var inter = setInterval(function () {
            timenumber = timenumber - 1;
            _this.lab_time.text = timenumber + "";
            if (timenumber == 0) {
                // alert('done')
                clearInterval(inter);
                _this.removeCircle();
                _this._startTime = egret.getTimer();
                _this.addCircles(function () { });
                _this.removeChild(_this.btn_start);
                _this.gameGroup.touchChildren = true;
                _this.removeChild(_this.gp_timeBox);
                _this.label_clock.text = "按1-25号的顺序依次点击糖果球";
                return;
            }
        }, 1000);
        this.btn_start.backgroundColor = 0xC3C1C1;
    };
    GameScene.prototype.onComplete = function () {
        var _this = this;
        this.stage.scaleMode = egret.StageScaleMode.FIXED_WIDTH;
        // 顶部提示文字
        this.label_clock.y = 80;
        this.label_clock.textAlign = egret.HorizontalAlign.CENTER;
        this.label_clock.x = 0;
        this.label_clock.width = GameData.STAGEWIDTH;
        this.label_clock.scaleY = 1 / App.BILIH;
        // 中间数字
        this.gameGroup.width = 600;
        this.gameGroup.height = 600;
        this.gameGroup.x = (GameData.STAGEWIDTH - this.gameGroup.width) / 2;
        this.gameGroup.y = this.label_clock.y + this.label_clock.height + 60;
        this.scaleY = App.BILIH;
        this.gameGroup.scaleY = 1 / App.BILIH;
        // 开始按钮
        this.btn_start.anchorOffsetX = this.btn_start.width / 2;
        this.btn_start.anchorOffsetY = this.btn_start.height / 2;
        this.btn_start.scaleY = 1 / App.BILIH * 0.8;
        this.btn_start.scaleX = .8;
        // alert(GameData.STAGEHEIGHT);
        this.btn_start.y = GameData.STAGEHEIGHT * (1 / App.BILIH) - 180;
        this.btn_start.x = GameData.STAGEWIDTH / 2;
        // 时间盒子
        this.gp_timeBox.scaleY = 1 / App.BILIH;
        this.gp_timeBox.width = GameData.STAGEWIDTH;
        this.gp_timeBox.x = 0;
        // 提示
        this.lab_title.width = GameData.STAGEWIDTH;
        this.lab_title.verticalCenter = -300;
        this.lab_title.textAlign = egret.HorizontalAlign.CENTER;
        this.lab_title.x = 0;
        // 数字
        this.lab_time.width = GameData.STAGEWIDTH;
        this.lab_time.verticalCenter = -100;
        this.lab_time.textAlign = egret.HorizontalAlign.CENTER;
        this.lab_time.x = 0;
        this.gameGroup.touchChildren = false;
        this.btn_start.visible = false;
        this.addCircles(function () {
            _this.addAnimation();
        });
        this.btn_start.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapStart, this);
    };
    GameScene.prototype.addCircles = function (callback) {
        var randarr = App.randomArr(this.numberArr);
        for (var i = 0; i < 5; i++) {
            for (var j = 0; j < 5; j++) {
                var circle = new Circle(randarr[i * 5 + j]);
                circle.x = i % 5 * this.gameGroup.width / 5;
                circle.y = j % 5 * this.gameGroup.height / 5;
                this.circleArr.push(circle);
                this.gameGroup.addChild(circle);
                circle.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapCircle, this);
            }
        }
        callback && callback();
    };
    GameScene.prototype.timeBox = function (scene, seconds) {
        var group = new eui.Group();
    };
    GameScene.prototype.addAnimation = function () {
        var _this = this;
        for (var i = 24; i >= 0; i--) {
            var circle = this.gameGroup.getChildAt(i);
            circle.y = -(circle.y + this.stage.stageHeight / 2);
        }
        var maxTime = 0;
        for (var i = 0; i <= 4; i++) {
            for (var j = 0; j <= 4; j++) {
                var circle = this.gameGroup.getChildAt(i * 5 + j);
                var time = (i * (5 - i) + (5 - j));
                var waitTime = 200 * (time - 1);
                maxTime = maxTime > waitTime ? maxTime : waitTime;
                var tween = egret.Tween.get(circle)
                    .wait(waitTime)
                    .to({ y: -circle.y - this.stage.stageHeight / 2 }, 2000, egret.Ease.backIn);
            }
        }
        // console.log(maxTime+)
        setTimeout(function () {
            _this.btn_start.visible = true;
        }, maxTime + 2400);
    };
    GameScene.prototype.removeEvent = function () {
        for (var i = 0; i < this.circleArr.length; i++) {
            this.circleArr[i].removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapCircle, this);
        }
    };
    GameScene.prototype.done = function () {
        var _this = this;
        this._endTime = egret.getTimer();
        var seconds = (this._endTime - this._startTime);
        this.removeEvent();
        var score = seconds / 1000;
        App.Post({
            do: 'play',
            params: {
                score: score,
                sign: App.getSign(score),
                openid: App.getUser().openid,
            },
        }, function (data) {
            if (data.code == 1) {
                var playparams = data.data;
                var user = App.getUser();
                user.result = playparams;
                App.setUser(user);
                location.href = Config.H5_FILYIN;
                // let gameover:GameOver  = new GameOver();
                // this.addChild(gameover) 
                _this.gameGroup.parent && _this.gameGroup.parent.removeChild(_this.gameGroup);
                _this.btn_start.parent && _this.btn_start.parent.removeChild(_this.btn_start);
                _this.lab_time.parent && _this.lab_time.parent.removeChild(_this.lab_time);
                _this.label_clock.parent && _this.label_clock.parent.removeChild(_this.label_clock);
            }
            else {
                alert(data.message);
            }
        });
    };
    GameScene.prototype.onTapCircle = function (e) {
        var group = e.currentTarget.getChildAt(0);
        var yesbg = group.getChildAt(0);
        var nobg = group.getChildAt(1);
        var bg = group.getChildAt(2);
        // this.done();
        var label = group.getChildAt(3);
        if (yesbg.name == this._currentNumber + "")
            return;
        if (Number(label.text) == this._currentNumber + 1) {
            this._currentNumber += 1;
            var numChildren = this.gameGroup.numChildren;
            for (var i = 0; i < numChildren; i++) {
                var subCls = this.gameGroup.$children[i];
                var yesbg_1 = subCls['yes'];
                var nobg_1 = subCls['no'];
                var cbg = subCls['bg'];
                var num_label = subCls['num_label'];
                num_label.textColor = 0x458FEB;
                yesbg_1.visible = false;
                nobg_1.visible = false;
                cbg.visible = true;
            }
            label.textColor = 0xffffff;
            bg.visible = false;
            yesbg.visible = true;
            yesbg.name = label.text;
            if (this._currentNumber == 25) {
                this.done();
            }
        }
        else {
            var numChildren = this.gameGroup.numChildren;
            for (var i = 0; i < numChildren; i++) {
                var subCls = this.gameGroup.$children[i];
                var cnobg = subCls['no'];
                var cbg = subCls['bg'];
                var ybg = subCls['yes'];
                var num_label = subCls['num_label'];
                if (!ybg.visible) {
                    cbg.visible = true;
                    num_label.textColor = 0x458FEB;
                }
                cnobg.visible = false;
            }
            nobg.visible = true;
            bg.visible = false;
            label.textColor = 0xffffff;
        }
    };
    //当场景在舞台上移除
    GameScene.prototype.onRemove = function () {
    };
    return GameScene;
}(eui.Component));
__reflect(GameScene.prototype, "GameScene");
