var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var Circle = (function (_super) {
    __extends(Circle, _super);
    function Circle(num) {
        var _this = _super.call(this) || this;
        _this._currentNumber = 0;
        _this._colorDefault = 0x608fe2;
        _this._colorError = 0xf5bd01;
        _this._colorTrue = 0xff2100;
        _this.addEventListener(eui.UIEvent.CREATION_COMPLETE, _this.onComplete, _this);
        _this.skinName = "resource/exml/CircleSkin.exml";
        _this._number = num + "";
        return _this;
    }
    Circle.prototype.onComplete = function () {
        this.num_label.text = this._number;
    };
    return Circle;
}(eui.Component));
__reflect(Circle.prototype, "Circle");
window['Circle'] = Circle;
