var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var StartScene = (function (_super) {
    __extends(StartScene, _super);
    function StartScene() {
        var _this = _super.call(this) || this;
        _this.addEventListener(eui.UIEvent.CREATION_COMPLETE, _this.onComplete, _this);
        _this.skinName = "resource/exml/StartSceneSkin.exml";
        return _this;
    }
    //当场景添加到舞台上
    StartScene.prototype.onComplete = function () {
        this.startBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapBtn, this);
    };
    StartScene.prototype.onTapBtn = function () {
        this.startBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapBtn, this);
        var gameScene = new GameScene();
        this.addChild(gameScene);
        // if(this1.parent){	
        // 	this1.parent.removeChild(this1);
        // }
    };
    //当场景在舞台上移除
    StartScene.prototype.onRemove = function () {
    };
    return StartScene;
}(eui.Component));
__reflect(StartScene.prototype, "StartScene");
window['StartScene'] = StartScene;
