var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var SocketManager = (function () {
    function SocketManager() {
    }
    SocketManager.getInstance = function () {
        if (SocketManager.webSocket) {
            SocketManager.close();
        }
        return new egret.WebSocket();
    };
    SocketManager.close = function () {
        SocketManager.webSocket.close();
    };
    SocketManager.connect = function () {
        this.webSocket = SocketManager.getInstance();
        this.webSocket.addEventListener(egret.ProgressEvent.SOCKET_DATA, this.onReceiveMessage, this);
        this.webSocket.addEventListener(egret.Event.CONNECT, this.onSocketOpen, this);
        this.webSocket.connect("echo.websocket.org", 80);
    };
    SocketManager.onSocketOpen = function () {
        console.log("连接成功，发送数据：");
    };
    SocketManager.send = function (cmd, callback) {
        this.callback = callback;
        this.webSocket.writeUTF(cmd);
    };
    SocketManager.onReceiveMessage = function (e) {
        var msg = this.webSocket.readUTF();
        this.callback(msg);
    };
    return SocketManager;
}());
__reflect(SocketManager.prototype, "SocketManager");
