// TypeScript file
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
// 公共方法
var App = (function () {
    function App() {
        App.callback = null;
    }
    App.getUrlloader = function () {
        if (this.urlloader) {
            return App.urlloader;
        }
        this.urlloader = new egret.URLLoader();
        return this.urlloader;
    };
    App.getUser = function () {
        return JSON.parse(window.localStorage.getItem('user'));
    };
    App.setUser = function (data) {
        return window.localStorage.setItem('user', JSON.stringify(data));
    };
    App.getSign = function (score) {
        var str = score + App.getUser().openid + Config.POST_KEY;
        var mdSign = new md5().hex_md5(str);
        return mdSign.substr(5, 6);
    };
    // 开始请求接口
    App.Post = function (obj, callback) {
        obj = obj || {};
        App.callback = callback || false;
        var dos = obj.do.includes('http') ? obj.do : (Config.REQUEST_URL + obj.do);
        var url = dos;
        var param = obj.params || {};
        var loader = App.getUrlloader();
        loader.addEventListener(egret.Event.COMPLETE, this.onPostComplete, this);
        // get请求
        for (var key in param) {
            if (param.hasOwnProperty(key)) {
                var element = param[key];
                url += '&' + key + '=' + element;
            }
        }
        // url += '&openid=oL6vown5JjH2LtdrD4zSjSALvSQY'
        var request = new egret.URLRequest(url);
        request.method = obj.method == 'get' ? egret.URLRequestMethod.GET : egret.URLRequestMethod.POST;
        request.method = egret.URLRequestMethod.GET;
        var variables = "";
        var ands = "";
        if (!obj.method) {
            for (var key in param) {
                if (param.hasOwnProperty(key)) {
                    var element = param[key];
                    variables += ands + key + '=' + element;
                    ands = "&";
                }
            }
        }
        App.test = !!obj.test;
        if (App.test) {
            console.log('参数');
            console.log(param);
        }
        request.data = new egret.URLVariables(variables);
        loader.load(request);
    };
    // 当完成请求时的回调
    App.onPostComplete = function (event) {
        var loader = event.target;
        var data = loader.data;
        data = typeof data == 'string' ? JSON.parse(data) : data;
        if (App.test) {
            console.log('返回');
            console.log(data);
        }
        App.callback && App.callback(data);
    };
    App.randomArr = function (arr) {
        var t;
        for (var i = 0; i < arr.length; i++) {
            var rand = (Math.random() * arr.length).toFixed(0);
            t = arr[rand];
            arr[rand] = arr[i];
            arr[i] = t;
        }
        return arr.filter(function (v) { return v; });
    };
    App.initGameInstance = function () {
        if (!App._initGame) {
            App._initGame = new initGame();
        }
        return App._initGame;
    };
    App.getGameScene = function () {
        if (!App._gameGame) {
            App._gameGame = new GameScene();
        }
        return App._gameGame;
    };
    App.urlloader = null;
    App._initGame = null;
    App._gameGame = null;
    App.reGame = 0;
    App.BILIW = 0; //屏幕宽比例
    App.BILIH = 0; //屏幕高比例
    return App;
}());
__reflect(App.prototype, "App");
