var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var AlertTimer = (function (_super) {
    __extends(AlertTimer, _super);
    function AlertTimer(seconds) {
        var _this = _super.call(this) || this;
        _this._seconds = seconds;
        _this.addEventListener(eui.UIEvent.CREATION_COMPLETE, _this.onComplete, _this);
        _this.skinName = "resource/exml/base/AlertTimerSkin.exml";
        return _this;
    }
    AlertTimer.prototype.onComplete = function () {
        var timer = new egret.Timer(1000, this._seconds);
        timer.addEventListener(egret.TimerEvent.TIMER, this.onTimer, this);
        timer.addEventListener(egret.TimerEvent.TIMER_COMPLETE, this.onStop, this);
        this._timer = timer;
    };
    AlertTimer.prototype.onTimer = function (e) {
        var target = e.currentTarget;
        this.lab_time.text = target.repeatCount - target.currentCount + "";
    };
    AlertTimer.prototype.start = function () {
        this._timer.start();
    };
    AlertTimer.prototype.onStop = function (callback) {
        this._timer.removeEventListener(egret.TimerEvent.TIMER, this.onTimer, this);
        this._timer.removeEventListener(egret.TimerEvent.TIMER_COMPLETE, this.onStop, this);
        this._timer = null;
        if (this.parent) {
            this.parent.removeChild(this);
        }
    };
    return AlertTimer;
}(eui.Component));
__reflect(AlertTimer.prototype, "AlertTimer");
