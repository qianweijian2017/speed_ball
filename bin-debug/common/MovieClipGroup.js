var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
// TypeScript file
/**
 * MvoieClipGroup
 */
var MovieClipGroup = (function (_super) {
    __extends(MovieClipGroup, _super);
    function MovieClipGroup() {
        var _this = _super.call(this) || this;
        _this.source = ""; //注意：[color=#000][font=微软雅黑,]自定义组建的属性要有默认值，才能被运行时exml解析器识别[/font][/color]
        return _this;
    }
    MovieClipGroup.prototype.createChildren = function () {
        _super.prototype.createChildren.call(this);
        var data = RES.getRes(this.source + "_json");
        var txtr = RES.getRes(this.source + "_png");
        var mcFactory = new egret.MovieClipDataFactory(data, txtr);
        var mc1 = new egret.MovieClip(mcFactory.generateMovieClipData(this.source));
        mc1.play(-1);
        this.addChild(mc1);
    };
    return MovieClipGroup;
}(eui.Group));
__reflect(MovieClipGroup.prototype, "MovieClipGroup");
