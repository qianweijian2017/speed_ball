var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var SoundManager = (function () {
    function SoundManager() {
    }
    SoundManager.instance = function () {
        return this._instance == null ? this._instance = new SoundManager() : this._instance;
    };
    /**
     * 初始化背景音乐
     */
    SoundManager.prototype.initBgSound = function () {
        if (this.bgChannel) {
            this.bgChannel.stop();
            this.bgChannel = null;
        }
        this.bgSound = RES.getRes("game_bg_mp3");
    };
    return SoundManager;
}());
__reflect(SoundManager.prototype, "SoundManager");
