var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var GameData = (function () {
    function GameData() {
    }
    //屏幕的适配方法
    GameData.adaptiveScreen = function (contentGroup) {
        if (!contentGroup) {
            return;
        }
        contentGroup.x = GameData.STAGEWIDTH / 2;
        contentGroup.y = GameData.STAGEHEIGHT / 2;
        contentGroup.anchorOffsetX = contentGroup.width / 2;
        contentGroup.anchorOffsetY = contentGroup.height / 2;
        //计算宽高比
        var scaleValue = Math.min((GameData.STAGEWIDTH / contentGroup.width), (GameData.STAGEHEIGHT / contentGroup.height), 1);
        //保留一位小数
        scaleValue = Math.floor(scaleValue * 10) / 10;
        //进行容器缩放
        contentGroup.scaleX = contentGroup.scaleY = scaleValue;
    };
    //屏幕的宽高
    GameData.STAGEWIDTH = 0;
    GameData.STAGEHEIGHT = 0;
    //预设的宽高
    GameData.PreinstallWidth = 720;
    GameData.PreinstallHeight = 1280;
    /**
     * 是否在游戏中 1:表示在游戏中  -1:表示不再游戏中
     */
    GameData.isGaming = -1;
    /**
     * 是否播放高潮音乐
     */
    GameData.playGaoChaoSound = false;
    /**
     * 是否停止音效
     */
    GameData.stopEffect = false;
    return GameData;
}());
__reflect(GameData.prototype, "GameData");
